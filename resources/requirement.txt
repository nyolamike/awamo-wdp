Web Developer Practical Interview
Develop a unit tested web application with a rich user interface [very closely similar/same
with shared GIF] that receives two numbers, an operation one of [ADD, SUB, MUL, DIV] from a
web form and submits them to a restful web API. The restful web API returns the value
resulting from applying the specified operation to the two numbers.
Restful Web API
The restful web API expects a post request and JSON payload of the structure below;
Where;
• expr is a string containing the expression to be evaluated.
• precision is the number of significant digits in formatted output. Undefined by default.
You don’t need to specify this.
The restful web API responds with a JSON payload of the structure below;
Where;
• result is a string with the results of the expression on success.
• error is an error message of type String. On success, error will be null.
All POST requests should be made to http://api.mathjs.org/v4/
{
"expr": "1.2 * 4.5",
"precision": 14
} {
"result": "5.4",
"error": null
}Results Table
Upon response of the restful web API, your web application is required to add a row to the
results table with columns as illustrated in the GIF. Also, the table rows should be removable,
when the remove icon is clicked.
Prior to addition of the response to the results table, the response should be processed as
below;
a) Generate a random number, and round it off.
b) If the result from a) is EQUAL to 1, override the response from the API and return an
updated response with the computation below;
o Generate another random number, multiply it by 4000, and find the ceiling of
the result
c) If the result from a) is NOT EQUAL to 1, return the response from the API as is.
Because the previous step could introduce wrong computations, your web application is
required to validate the response against a self-computed value. If the response does not
match your web application’s expected value, the web application is required to mark the row
as a failure (See GIF for illustration).
Note: Please note the word “response” is synonymous to the JSON field “result” returned by
the restful web API on successful computation.
Evaluation Criteria
• UI/UX MUST exactly/very closely match that presented in the GIF
• Error handling both from web form and restful web API is a MUST
• Use of any libraries of your choice (if necessary)
• Ability to write readable, modular and testable code
• All work MUST be accompanied with unit tests; use any testing frameworks of your
choice
• Use of version control is a MUST. All written code should be submitted via a link to any
VCS of your liking.
• Use of build tools, CI/CD tools, anything else fancy is a plus.
• Web applications that do not meet these criteria will not be considered
Good Luck Buddy