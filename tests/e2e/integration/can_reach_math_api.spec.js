describe('Reach Math API',function(){
    beforeEach(() => {
        // before each individual test instead of
        // repeating it in every test.
        cy.visit('/');
    });


    it('It can reach the math api, error is null', () => {
        cy.request({
            method: 'POST',
            url: 'https://api.mathjs.org/v4/', 
            form: false, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
            body: {
                expr: "1.2 * 4.5",
                precision: 3
            }
        }).then((response) => {
            expect(response.status).to.eq(200)
            expect(response.body).to.have.property('result')
            expect(response.body).to.have.property('error',null)
        })
    })

    it('should fail', () => {
        cy.request({
            method: 'POST',
            url: 'https://api.mathjs.org/v4/', 
            form: false, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
            body: {
                expr: "badinput * 4.5",
                precision: 3
            },
            failOnStatusCode: false,
        }).then((response) => {
            expect(response.status).to.eq(400)
            expect(response.body).to.have.property('result',null)
            expect(response.body).to.have.property('error')
        })
    })



})