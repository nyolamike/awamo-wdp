describe('Domain Unit tests',function(){
    beforeEach(() => {
        // before each individual test instead of
        // repeating it in every test.

        cy.on('uncaught:exception', (err) => {
            expect(err.message).to.include('Ignoring error for now');
            return false;
        });

        cy.visit('/');
    });

    it('turns a form operation object to string expression',function(){
        var arg = {
            numberOne:4,
            numberTwo:5,
            operation:"+"
        };
        var argStr = JSON.stringify(arg);
        cy.get('[data-invoke="operationToExpression"]')
            .invoke('attr', 'data-arg', argStr)
        cy.get('[data-invoke="operationToExpression"]')
            .click({ force: true })
        cy.get('[data-invoke="operationToExpression"]')
            .should(($div) => {
                expect($div).to.have.html('4 + 5')
            })
            
    })

    it('should return an empty string',function(){
        cy.get('[data-invoke="operationToExpression"]')
            .invoke('attr', 'data-arg', null)
        cy.get('[data-invoke="operationToExpression"]')
            .click({ force: true })
        cy.get('[data-invoke="operationToExpression"]')
            .should(($div) => {
                expect($div).to.have.html('')
            })
        
        var argStr = JSON.stringify({});
        cy.get('[data-invoke="operationToExpression"]')
            .invoke('attr', 'data-arg', argStr)
        cy.get('[data-invoke="operationToExpression"]')
            .click({ force: true })
        cy.get('[data-invoke="operationToExpression"]')
            .should(($div) => {
                expect($div).to.have.html('')
            })
    })

    it('generate a random number and round it off',function(){
        cy.on('uncaught:exception', (err) => {
            expect(err.message).to.include('Ignoring this error for now');
            return false;
        });

        cy.get('[data-invoke="genrateRandom"]')
            .click({ force: true })

        cy.get('[data-invoke="genrateRandom"]').invoke('text').then((text) => {
            var res = text.trim();
            expect(res).not.to.be.empty
            expect(['0','1','2','3','4']).to.include(res)
        });
    })

    it('generate another random number wth ceil @4000',function(){
        cy.on('uncaught:exception', (err) => {
            expect(err.message).to.include('Ignoring this error for now2');
            return false;
        });

        cy.get('[data-invoke="genrateAnotherRandom"]')
            .click({ force: true })
        cy.get('[data-invoke="genrateAnotherRandom"]').invoke('text').then((text) => {
            var res = text.trim();
            expect(res).not.to.be.empty
            //should be x >= 0 &&  x < 4000
            var resNum = parseFloat(res)
            assert.isAbove(resNum, -1, 'is greater than or equal to 0')
            assert.isBelow(resNum, 4000, 'is less than 4000')
        });
    })

    it('priorProcess should alter if random is one',function(){
        cy.on('uncaught:exception', (err) => {
            expect(err.message).to.include('Ignoring this error for now2');
            return false;
        });
        var apiRes = 34;
        //we can only expect this function to return a number
        cy.get('[data-invoke="priorProcess"]')
            .invoke('attr', 'data-arg', apiRes)
        cy.get('[data-invoke="priorProcess"]')
            .click({ force: true })
        cy.get('[data-invoke="priorProcess"]').invoke('text').then((text) => {
            var res = text.trim();
            expect(res).not.to.be.empty
            //should be x >= 0 &&  x < 4000
            var resNum = parseFloat(res)
            assert.isNumber(resNum, ' is numeric')
        });
    })


    it('selfCompute 4 + 5 = 9',function(){
        cy.on('uncaught:exception', (err) => {
            expect(err.message).to.include('Ignoring this error for now2');
            return false;
        });
        var arg = {
            numberOne:4,
            numberTwo:5,
            operation:"+"
        };
        var argStr = JSON.stringify(arg);
        //we can only expect this function to return a number
        cy.get('[data-invoke="selfCompute"]')
            .invoke('attr', 'data-arg', argStr)
        cy.get('[data-invoke="selfCompute"]')
            .click({ force: true })
        cy.get('[data-invoke="selfCompute"]').invoke('text').then((text) => {
            var res = text.trim();
            expect(res).not.to.be.empty
            var resNum = parseFloat(res)
            assert.isNumber(resNum, ' is numeric')
            expect(resNum).to.equal(9)
        });
    })

    it('selfCompute 4 - 5 = -1',function(){
        cy.on('uncaught:exception', (err) => {
            expect(err.message).to.include('Ignoring this error for now2');
            return false;
        });
        var arg = {
            numberOne:4,
            numberTwo:5,
            operation:"-"
        };
        var argStr = JSON.stringify(arg);
        //we can only expect this function to return a number
        cy.get('[data-invoke="selfCompute"]')
            .invoke('attr', 'data-arg', argStr)
        cy.get('[data-invoke="selfCompute"]')
            .click({ force: true })
        cy.get('[data-invoke="selfCompute"]').invoke('text').then((text) => {
            var res = text.trim();
            expect(res).not.to.be.empty
            var resNum = parseFloat(res)
            assert.isNumber(resNum, ' is numeric')
            expect(resNum).to.equal(-1)
        });
    })

    it('selfCompute 0.666 * 0.23  = 0.153',function(){
        cy.on('uncaught:exception', (err) => {
            expect(err.message).to.include('Ignoring this error for now2');
            return false;
        });
        var arg = {
            numberOne:0.666,
            numberTwo:0.23,
            operation:"*"
        };
        var argStr = JSON.stringify(arg);
        //we can only expect this function to return a number
        cy.get('[data-invoke="selfCompute"]')
            .invoke('attr', 'data-arg', argStr)
        cy.get('[data-invoke="selfCompute"]')
            .click({ force: true })
        cy.get('[data-invoke="selfCompute"]').invoke('text').then((text) => {
            var res = text.trim();
            expect(res).not.to.be.empty
            var resNum = parseFloat(res)
            assert.isNumber(resNum, ' is numeric')
            expect(resNum).to.equal(0.153)
        });
    })

    it('selfCompute 2 / 3 = 0.667',function(){
        cy.on('uncaught:exception', (err) => {
            expect(err.message).to.include('Ignoring this error for now2');
            return false;
        });
        var arg = {
            numberOne:2,
            numberTwo:3,
            operation:"/"
        };
        var argStr = JSON.stringify(arg);
        //we can only expect this function to return a number
        cy.get('[data-invoke="selfCompute"]')
            .invoke('attr', 'data-arg', argStr)
        cy.get('[data-invoke="selfCompute"]')
            .click({ force: true })
        cy.get('[data-invoke="selfCompute"]').invoke('text').then((text) => {
            var res = text.trim();
            expect(res).not.to.be.empty
            var resNum = parseFloat(res)
            assert.isNumber(resNum, ' is numeric')
            expect(resNum).to.equal(0.667)
        });
    })

    // it('roundTo 0.6666666666666666, 3 = 0.667',function(){
    //     cy.on('uncaught:exception', (err) => {
    //         expect(err.message).to.include('Ignoring this error for now2');
    //         return false;
    //     });
    //     var arg = {
    //         n:0.6666666666666666,
    //         digits:3
    //     };
    //     var argStr = JSON.stringify(arg);
    //     //we can only expect this function to return a number
    //     cy.get('[data-invoke="roundTo"]')
    //         .invoke('attr', 'data-arg', argStr)
    //     cy.get('[data-invoke="roundTo"]')
    //         .click({ force: true })
    //     cy.get('[data-invoke="roundTo"]').invoke('text').then((text) => {
    //         var res = text.trim();
    //         expect(res).not.to.be.empty
    //         var resNum = parseFloat(res)
    //         assert.isNumber(resNum, ' is numeric')
    //         expect(resNum).to.equal(0.667)
    //     });
    // })



})