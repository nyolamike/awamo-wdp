describe("Validate Form Input and Display Error message",function(){
    beforeEach(() => {
        cy.visit('/');
    });

    it("Validates Number One: Required", function(){
        cy.get('[data-test="numberOne"]')
            .type('___')
            .clear();

        cy.get('[data-test="postOperationBtn"]')
            .click()
        
        cy.get('[data-test="numberOne"]')
            .should('have.value','');

        cy.get('[data-test="formError"]')
            .should('be.visible');
    })

    it("Shows error Number Two: Required", function(){
        cy.get('[data-test="numberTwo"]')
            .type('___')
            .clear() 

        cy.get('[data-test="postOperationBtn"]')
            .click()
       
        cy.get('[data-test="numberTwo"]')
            .should('have.value','');

        cy.get('[data-test="formError"]')
            .should('be.visible');

    })

    it("Shows error Number Two: Required", function(){
        cy.get('[data-test="numberTwo"]')
            .type('xx')

        cy.get('[data-test="postOperationBtn"]')
            .click()
       
        cy.get('[data-test="numberTwo"]')
            .should('have.value','xx');
    })

    it("Shows error Number One: Required", function(){
        cy.get('[data-test="numberOne"]')
            .type('xx')

        cy.get('[data-test="postOperationBtn"]')
            .click()
       
        cy.get('[data-test="numberOne"]')
            .should('have.value','xx');
    })

    

  
})