describe('Get And Send FOrm Data',function(){
    beforeEach(() => {
        // before each individual test instead of
        // repeating it in every test.
        cy.visit('http://127.0.0.1:8080/');
        cy.server()
    });


    it('Inserts some data and clicks the post button', () => {

        cy.get('[data-test="numberOne"]')
            .type('4');

        cy.get('[data-test="numberTwo"]')
            .type('5');

        cy.get('[data-test="operation"]')
            .select('+');

        cy.route({
            method: 'POST',
            url: 'https://api.mathjs.org/v4/',
            status: 200,
            response: {
                result: 2,
                error: null
            },
            delay: 500,
            onRequest: (xhr) => {
                // do something with the
                // raw XHR object when the
                // request initially goes out
            },
            onResponse: (xhr) => {
                // do something with the
                // raw XHR object when the
                // response comes back
            }
        }).as("mathApiCall")
        cy.get('[data-test="postOperationBtn"]')
            .click()
        cy.wait("@mathApiCall")

        cy.get('[data-test="numberOne"]')
            .should('have.value','4');

        cy.get('[data-test="numberTwo"]')
            .should('have.value','5');
        
        cy.get('[data-test="operation"]')
            .should('have.value','+'); 
    });

    it('should fail and display an error message', () => {

        cy.get('[data-test="numberOne"]')
            .type('4');

        cy.get('[data-test="numberTwo"]')
            .type('~~');

        cy.get('[data-test="operation"]')
            .select('+');

        // cy.route({
        //     method: 'POST',
        //     url: 'https://api.mathjs.org/v4/',
        //     status: 500,
        //     response: {
        //         error: "Internal Server Error",
        //         result: null
        //     },
        //     delay: 500
        // }).as("mathApiFailCall")
        cy.get('[data-test="postOperationBtn"]')
            .click()
        //cy.wait("@mathApiFailCall")

        cy.get('[data-test="numberOne"]')
            .should('have.value','4');

        cy.get('[data-test="numberTwo"]')
            .should('have.value','~~');
        
        cy.get('[data-test="operation"]')
            .should('have.value','+'); 

        cy.get('[data-test="formError"]')
            .should('be.visible'); 
    });

})