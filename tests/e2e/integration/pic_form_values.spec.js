describe('Picking Form Values',function(){
    beforeEach(() => {
        // before each individual test instead of
        // repeating it in every test.
        cy.visit('http://127.0.0.1:8080/');
    });


    it('gets inputs from form fields', () => {

        cy.get('[data-test="numberOne"]')
            .type('4');

        cy.get('[data-test="numberTwo"]')
            .type('5');

        cy.get('[data-test="operation"]')
            .select('+');
        
        cy.get('[data-test="postOperationBtn"]')
            .click()

        cy.get('[data-test="numberOne"]')
            .should('have.value','4');

        cy.get('[data-test="numberTwo"]')
            .should('have.value','5');
        
        cy.get('[data-test="operation"]')
            .should('have.value','+');
        
    });
})