describe('When a remove icon is clicked it should remove the row', function(){
    beforeEach(() => {
        cy.visit('/');
        cy.server()
    });

    it("Inserts one item and removes one item",function(){
        cy.get('[data-test="numberOne"]')
            .type('4');

        cy.get('[data-test="numberTwo"]')
            .type('5');

        cy.get('[data-test="operation"]')
            .select('+');

        cy.route({
            method: 'POST',
            url: 'https://api.mathjs.org/v4/',
            status: 200,
            response: {
                result: 2,
                error: null
            },
            delay: 500,
            onRequest: (xhr) => {
                // do something with the
                // raw XHR object when the
                // request initially goes out
            },
            onResponse: (xhr) => {
                // do something with the
                // raw XHR object when the
                // response comes back
            }
        }).as("mathApiCall")
        cy.get('[data-test="postOperationBtn"]')
            .click()
        cy.wait("@mathApiCall")

        cy.get('[data-test="numberOne"]')
            .should('have.value','4');

        cy.get('[data-test="numberTwo"]')
            .should('have.value','5');
        
        cy.get('[data-test="operation"]')
            .should('have.value','+'); 

        cy.get('[data-test="resultsTableBody"]')
            .find('tr').should('have.length', 1)

        cy.get('[data-test="resultsTableBody"]')
            .find('tr i').click()

        cy.get('[data-test="resultsTableBody"]')
            .find('tr').should('have.length', 0)
    })
})